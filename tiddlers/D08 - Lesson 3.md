D07 - Lesson 3
==========

- **Data access workflow**:
 - **1. Datastore**: abstract level to connect azure storage.
 - **2. Dataset**: some specific reference of data (It don't duplicate data).
 - **3. Data monitor**: can detect change in the data (data drift) to re-train the model and don't lose accuracy.
- **Lab create and version a dataset:**
- **Feature engineering:** Create new useful features.
- **Feature selection:** Some features are more important that others. 
- **Dimensionality reduction:** Some times a ML model don't need a lot of features.

D09 - Lesson 3
==========

- **Feature engineering:** Aggregations, part-of, binning, flagging, frequency-realte, embedding, derivate.
- **Feature selection:** Delete correlate features, delete irrelevant features, dimensionality reduction (PCA - Principal Component Analysis).

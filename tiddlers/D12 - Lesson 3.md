D12 - Lesson 3
==========

- **Data Drift:** Change in the meaning of data, It is the main cause of lost of accuracy in time.
 - **How to monitor data drift?**:
    - Define a baseline dataset.
    - Define a target dataset.
    - Check the % of change by feature, 0% No change - 100% All change.
- **Train a Model concepts**:
 - The function of the train process is find the **parameters** (i. e. parameters of general form of linear regression form).
 - The **hyperparameters** should be given by us before the training process. (The learning rate of the model)
- **Splitting Data:**:
 - **Training Data:** Find parameters.
 - **Validation Data:**  tunning hyperparameters.
 - **Test Data:** Check the performance of the final trained model.

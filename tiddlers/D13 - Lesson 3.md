D13 - Lesson 3
==========

    +-Taxonomy of Azure ML workspace
    |---- Compute Instances
    |---- Experiments
    |    |---- Run
    |---- Pipelines
    |---- Datasets
    |---- Compute targets

- **Training Classifiers:** Output is categorical or discrete.
 - Binary Classification
 - Multi-class single-label classification
 - Multi-class multi-label classification
 - Algorithms: 
    - Logistic Regression
    - Support Vector Machine
- **Training Regressors**: Output is numerical or continue
 - Regression to arbitrary values
 - Regression to values between 0 an 1
 - Algorithms:
    - Linear Regressor
    - Decision Forest Regressor
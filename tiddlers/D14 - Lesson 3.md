D14 - Lesson 3
==========

- **Evaluation Model performance:** split the data randomly to have the same statistical characteristics.
- **Confusion Matrix:** explained nicely with cats and dogs. Actual class (columns) vs predicted class (rows)
- **Evaluation metrics for classification**: Accuracy, precision, recall, f1 score.
  - **ROC**: **R**eceiver **O**perating **C**haracteristics **(ROC)**
  - **AUC**: **A**rea **U**nder the **C**urve
- **Evaluation metrics for regressors:** R-Squared, RMSE, MAE, Spearman correlation
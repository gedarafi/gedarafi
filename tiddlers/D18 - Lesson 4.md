D18 - Lesson 4
==========

- **Supervised Learning: Regression**
  - Linear Regression (Fast training, linear model)
  - Decision Forest (Fast training, Accurate)
  - Neural Net Regression (long training, Accurate)
- **Unsupervised Learning**
- **Semi-Supervised learning**
- **Clustering**
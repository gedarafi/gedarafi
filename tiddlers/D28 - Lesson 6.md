D28 - Lesson 6
==========

- **Programmatically Accessing Managed Services:** Azure Machine Learning service supports:
 - Scikit-learn
 - Tensorflow
 - PyTorch
 - Keras
* **Azure ML Python SDK:** include:
 - Manage datasets
 - Organize and monitor experiments
 - Model training
 - Automated Machine Learning

```

        # subscription_id
        !az account show --query id         
        
        # resource group
        !az group list --query [].name

```


# Ref:

* https://docs.microsoft.com/en-us/python/api/overview/azure/ml/?view=azure-ml-py
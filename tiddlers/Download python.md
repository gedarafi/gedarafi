- It is useful to use management packages like conda, this solve dependences automatically.
 - [Miniconda](https://docs.conda.io/en/latest/miniconda.html): Minimal instalation.
 - [Anaconda](https://www.anaconda.com/distribution/): Contain all packages required.

If you use miniconda, you can install tools in anaconda prompt with the commands:

    conda install spyder
    conda install jupyter_lab

It's important work with [environments](Works with enviroments python - conda)
D07 - Finishing Lesson 2
================

# **Steps Lab 1:**
 - Import dataset nyc-taxi
 - Split the dataset in training and testing
 - Train a model using a linear regression algorithm
 - Score the model, see the predict values vs the real values
 - Evaluate the model, check some useful statistics

 
# Next
- **Learning function:** 
    - Y = f(X) + e; e is irreducible error
    - irreducible error != model error
- **Parametric vs Non-parametric algorithms:** parametric (linear regression, multiple linear regression), non-parametric (KNN, Desicion Tree)
- **Classical ML vs Deep Learning:** All Deep learning algorithms are ML.
- **Approaches to ML**: Supervise, Unsupervised and Reinforcement.
- **The trade-Offs:** 
  - Bias vs variance 
  - Overfitting vs underfitting. 
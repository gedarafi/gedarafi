# ** General: **

# Title `#`
## Subtitle `##`

# Usefull:

---
- **Bold** `**Bold**`
- *italic* `*italic*`

---
1. Order list
1. Second item
    1. Sublist
    1. subsecond item
1. Last item

---

### **Images**

* Add images: 
  * `![Description](/path/file.png)`
---

# **In Tiddlywiki:**

* `[link text](#TiddlerTitle)`

##**Links:**

* Simple link 
 * <https://www.youtube.com/> `<https://www.youtube.com/>`
* hide link 
 * [youtube](https://www.youtube.com/) `[youtube](https://www.youtube.com/)`
* Local files
  * `[Description](/path/file.png)`

---

# Tables

`| Heading | Heading |
| ----| ----|
| text   | text |
| text | text |
`

---

Ref:
===
* [youtube](https://www.youtube.com/watch?v=f49LJV1i-_w)
* [guide](https://www.markdownguide.org/basic-syntax/)
* [IBM Cheatsheet](https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/markd-jupyter.html)
* [Order list](https://meta.stackexchange.com/questions/85474/how-to-write-nested-numbered-lists)
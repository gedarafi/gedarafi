created: 20190322222021029
creator: gedarafi
modified: 20220607113940202
modifier: gedarafi
tags: unix-like
title: grep
type: text/vnd.tiddlywiki

''g''lobally search a ''r''egular ''e''xpression and ''p''rint

```bash
# search word 'upload_max_filesize' in all files in the current directory
$ grep -i upload_max_filesize *

# search word 'upload_max_filesize' in all files recursively in all directories
$ grep -ri upload_max_filesize *

# Not maching all start with #
$ grep -v '^#'

# Using regex to check OR conditions
$ grep -E "contains this|OR this"

```

* `- i`, --ignore-case: don't disting between uppercase and lowercase
* `-r`, : recursively inside directories
* `-n`: display the number of the line.
* `-c`: count the number of matching lines
* `-v`, --invert-match: non-matching lines
* `-w`, --word-regexp: match the whole word.
* `-A`, --After: among of lines after the match 
* `-B`, --Before: among of lines before the match
* -o, -- :output the among of times appears the word searched.
* - E, --extened-regex: enable extended regular expression, (note regex don't return vales store in (), better use sed.
* `-l` --files-with-matches: display the files that contains the word

!! Common tasks

```bash
# Using grep to the output of another grep
$ cat tmp.txt
PCA 1 img\n
pca 2 nad\n
PCA 3 other\n
stuff 4
bla bla

# Lookup for PCA word but not img in the same line
$ grep PCA tmp.txt | grep -v img
PCA 3 other\n

# Search recursively files that contains the word word
$ grep -rl word .

```

! Q&A

*https://stackoverflow.com/questions/15758701/grepping-for-exact-words-with-unix
*https://stackoverflow.com/questions/12444808/how-do-i-fetch-lines-before-after-the-grep-result-in-bash
* https://unix.stackexchange.com/questions/26235/how-can-i-cat-a-file-and-remove-commented-lines
